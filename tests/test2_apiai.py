import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'lib'))

from assistant import Assistant

import time
from datetime import datetime

def main():

    assistant = Assistant()

    with open("test2_apiai.txt", "a") as f:
        f.write("\n\n=== {} ===\n\n".format(datetime.now()))

    try:
        while True:
            try:
                query = raw_input("Query: ")

                start = time.time()
                response = assistant.respond( query )
                end = time.time()

                print("Action: {}\n".format(response['action'], end - start))

                with open("test2_apiai.txt", "a") as f:
                    f.write("{:<30}  {:<40}  {:<20}  {:.2}\n".format(query, response['action'], response['params'], end - start))

            except:
                raise
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    main()
