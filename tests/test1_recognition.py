import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'lib'))

# from speaker import Speaker


import speech_recognition as sr
import time
from datetime import datetime

def main():

    r = sr.Recognizer()
    m = sr.Microphone()

    with open("test1_result.txt", "a") as f:
        f.write("\n\n=== {} ===\n\n".format(datetime.now()))

    try:
        print("A moment of silence, please...")
        with m as source: r.adjust_for_ambient_noise(source, duration = 5)
        print("Set minimum energy threshold to {}".format(r.energy_threshold))

        while True:
            print("Say something!")
            with m as source: audio = r.listen(source)
            print("Got it! Now to recognize it...")

            # write audio to a WAV file
            with open("records/record.wav", "wb") as f:
                f.write(audio.get_wav_data())

            try:
                start = time.time()
                # recognition = r.recognize_sphinx(audio)
                recognition = r.recognize_google(audio)
                end = time.time()
                print("Me: {}\n".format(recognition, end - start))

                with open("test1_result.txt", "a") as f:
                    f.write("{:<20}  {:.2}\n".format(recognition, end - start))

            except sr.UnknownValueError:
                print("Oops! Didn't catch that")
            except sr.RequestError as e:
                print("Uh oh! Couldn't request results: {0}".format(e))
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    main()
