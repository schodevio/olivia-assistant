import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'lib'))

from gtts import gTTS

import time
from datetime import datetime

def main():

    with open("test3_tts.txt", "a") as f:
        f.write("\n\n=== {} ===\n\n".format(datetime.now()))

    try:
        while True:
            try:
                text = raw_input("Text: ")

                start = time.time()
                tss = gTTS( text = text, lang = 'en')
                tss.save( 'records/speech.mp3' )
                end = time.time()

                os.system("mpg321 {}".format( 'records/speech.mp3' ))

                with open("test3_tts.txt", "a") as f:
                    f.write("{:.2}     {}\n".format(end - start, text))

            except:
                raise
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    main()
