


=== 2017-09-07 22:32:00.761478 ===

0.73     Hello World
1.6     Hurricane Irma is an extremely powerful tropical cyclone affecting the Leeward Islands and Puerto Rico, and threatening Cuba and the Southeastern United States.
0.89     The last updates to this article may not reflect the most current information about this tropical cyclone for all areas.
0.6     He is a state congressman for the tenth district of Tennessee, having been reelected in 2012.
0.76     Fife Opera has a large proportion of younger singers, and increasingly is geared towards outreach and touring.
1.2     Within minutes of the launch, the first stage of the Dragon rocket landed back at the Kennedy Space Center as planned.
1.5     Traditionally, the federal government has regulated vehicle safety, and states have handled vehicle operations, such as issuing driver licenses.
0.8     The cap would gradually grow to 100,000 after three years. 
0.56     Autonomous vehicle regulation remains a hot topic with many moving parts.
1.0     Automakers and tech companies are spending billions to develop technology that is expected to be worth trillions.


=== 2017-09-07 22:40:30.222460 ===

1.2     Cities around the world are playing host to some of the smartest innovation around, providing a glance at what could become a part of modern-day culture in the years to come.
0.79     A city once known for the steel industry has become a hotbed of self-driving car research.
1.6     The city, known for its three rivers, has hundreds of bridges, which can be especially challenging for autonomous vehicles.
0.78     Computer scientists from the University of Waterloo created an app that directs users to take better selfies.
1.5     A favorite of early tech adopters, the smooth wall-mounted disk was a conversation piece as much as a heating and cooling device.
0.82     When the motion sensor is triggered, the Nest will light up with large orange numbers showing the current temperature.
2.0     You may be using your smartphone as your primary camera, but there are a slew of new gadgets for capturing and sharing your life.
1.2     Such immersive images have been taking off on services like Facebook and YouTube, where people can swipe or drag to move around the scene on their own.
0.63     That is, if a new toy from robotics company Sphero is any indication.
0.79     It features blue and red lights that illuminate when the head is attached to the body.


=== 2017-09-07 22:47:03.689032 ===

1.4     Google Assistant, the nameless voice-activated helper that powers the Google Home smart speaker, will appear in third-party devices staring later this year.
1.5     Smart speakers use always-listening microphones to detect their trigger word, then process and reply to voice commands in natural language.
1.2     They're competing to becoming the primary voice operating system, and want to see their assistants in other devices like cars, thermostats and other products.
0.84     Consumers may be more likely to adopt smart home products and corresponding voice assistants if they are able to work together. 
0.89     One of the biggest challenges the smart home industry currently faces is how many products are siloed.
0.97     It's clear the company wants to get this latest version right; rather than throw new features against the wall to see what sticks.
0.71     Some replacement phones also overheated.
1.7     Before you assume I'm a neglectful parent who missed out on early bonding moments with my baby, stay with me: It made me a better mother.
0.73     You place the baby into a sleeper outfit securely attached in the bassinet.
0.88      It would require any companies that provide the federal government with internet-enabled devices to meet basic security requirements.


=== 2017-09-07 22:57:20.309390 ===

0.86     The new hardware takes clearer panoramic photos, while two high definition cameras capture detailed images of shops and street signs.
1.1     It said it had captured images of more than 10 million miles of road, and more than 80 billion photos.
0.7     The cars are also equipped with a laser radar system to help judge depth.
1.1     You might not know their names, but they are just two of the female inventors behind everyday objects and scientific innovations.
0.51     Tech companies are competing to develop the first viable passenger-carrying sky taxis.
0.87     Airbus, the French aircraft maker, is also working on a prototype air taxi.
1.4     Understanding decomposition can hold the key to solving murders, finding missing people and crucially recognising them, and that is why "body farms" exist.
1.2     Body farms are essentially outdoor laboratories where experiments using donated human cadavers investigate taphonomy - the science of decomposition.
0.51     So decomposition is anything but simple.
0.99     The hardest hitters on Earth are highly likely to take you by surprise, and not just because of their speed and skill.


=== 2017-09-07 23:08:03.658659 ===

1.1     This is the next stage in our rapid evolution from an idea to the production of a commercially successful aircraft that will revolutionize the way we travel in and around the world's cities
0.61     In the world of anonymous apps, things never stay nice for long.
0.78     But the built-in cue to play nice isn't entirely working.
1.2     The cutting edge of drone delivery isn't one of the usual technology hotspots.
0.74     The government expects to save lives thanks to faster delivery of medical supplies.
0.74     Tanzania will open four drone distribution centers with Silicon Valley startup.
1.1     I felt that there was this underdeveloped market that I could potentially use the skills and experience that I've gained abroad to help develop further.
1.7     And he had to work around sanctions that left the country with few links to international banking and no credit card networks.
2.0     Each year, tens of thousands of Indian students head to the U.S. in order to get a college degree, find a job and pursue the American Dream.
1.2     But there's also an undercurrent of nervousness and anxiety that is spurring more Indian expatriates to return home as well.
