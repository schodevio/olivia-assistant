import speech_recognition as sr

from lib.speaker import Speaker
from lib.assistant import Assistant
from lib import utils

def main():

    r = sr.Recognizer()
    m = sr.Microphone()

    assistant = Assistant()
    olivia = Speaker()

    active = True
    wakeup_words = ["hello", "hi", "hey", "good morning", "olivia"]
    sleeping_words = ["thank you", "thank you Olivia" "bye", "goodbye", "that's all"]

    try:
        print("A moment of silence, please...")
        with m as source: r.adjust_for_ambient_noise(source, duration = 5)
        print("Set minimum energy threshold to {}".format(r.energy_threshold))

        while True:
            print("Say something!")
            with m as source: audio = r.listen(source)
            print("Got it! Now to recognize it...")

            # write audio to a WAV file
            with open("records/record.wav", "wb") as f:
                f.write(audio.get_wav_data())

            try:
                recognition = (r.recognize_sphinx(audio), r.recognize_google(audio))[active]
                print("Me: {}".format(recognition))

                if not active:
                    if recognition in wakeup_words:
                        active = True

                        response = assistant.respond( recognition )
                        output = utils.speech_response(response)

                        print( output )
                        olivia.speak( output )

                else:
                    if recognition in sleeping_words:
                        active = False

                    response = assistant.respond( recognition )
                    output = utils.speech_response(response)

                    print( output )
                    olivia.speak( output )


            except sr.UnknownValueError:
                print("Oops! Didn't catch that")
            except sr.RequestError as e:
                print("Uh oh! Couldn't request results: {0}".format(e))
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    main()
