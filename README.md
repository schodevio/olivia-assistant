# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
  Sample software for Master Diploma Thesis - Python implementation of voice interaction in smart home
* Version
  1.0
* License
  MIT

### How do I get set up? ###

1. Install all dependencies (it may happen that app require other system libs, just google it)
2. Set your API keys (OWM & API.AI)
3. Plug microphone
4. Run
