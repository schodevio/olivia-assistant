from gtts import gTTS
import os

class Speaker:

    def __init__( self, lang = 'en' ):
        self.lang = lang

    def speak( self, text = '' ):
        try:
            tss = gTTS( text = text, lang = self.lang )
            tss.save( 'records/speech.mp3' )
            os.system("mpg321 {}".format( 'records/speech.mp3' ))
        except:
            pass
