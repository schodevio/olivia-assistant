from datetime import datetime
import pytz
import os
import random
import requests
import pyowm

HOST = 'http://0.0.0.0:8080/'


def speech_response(response):
    if response['speech'] == '':
        return eval( "{0}({1})".format(response['action'], response['params']) )
    else:
        return( response['speech'] )


def get_time(params):
    dt_now_utc = datetime.now(tz = pytz.UTC)
    time_now = dt_now_utc.astimezone(pytz.timezone('Europe/Warsaw'))
    return 'It is {0:%-I}:{0:%M} {0:%p}.'.format(time_now)


def get_date(params):
    today = datetime.now().date()

    if params['date'] == "":
        return "It is {d.day} of {d:%B} {d.year}".format(d = today)

    params_date = datetime.strptime(params['date'], "%Y-%m-%d").date()

    if params_date > today:
        return "It will be {d.day} of {d:%B} {d.year}".format(d = params_date)
    elif params_date < today:
        return "It was {d.day} of {d:%B} {d.year}".format(d = params_date)


def get_weather_info(params):

    owm = pyowm.OWM('OWM_KEY', language = 'en')

    try:
        city = params['location']['city']
    except:
        if owm.is_API_online() and params['location'] == "":
            city = "Krakow"
        elif owm.is_API_online():
            return "Sorry, but I need name of the city."
        else:
            return "Sorry, but I cannot check it."

    weather = owm.weather_at_place(city).get_weather()

    result = "Weather in {}: ".format(city)
    result += "temperature: {} degrees, ".format(int(weather.get_temperature(unit = 'celsius')['temp']))
    result += "clouds: {} percent, ".format(weather.get_clouds())
    result += "wind speed: {} meters per second, ".format(int(weather.get_wind()['speed']))
    result += "humidity: {} percent.".format(weather.get_humidity())

    return result


equipment = {
    'computer': 1,
    'coffee machine': 2,
    'microwave': 3,
    'tv': 4,
    'video player': 5,
    'radio': 6,
    'lamp': 7,
    'heating': 8,
    'air conditioner': 9
}


def switch_device_on(params):

    try:
        devices = params['device']

        for device in devices:
            print('Device no. {}'.format(equipment[device]))

            url = '{}devices/{}/'.format(HOST, equipment[device])
            data = {"name": device, "status": True}
            r = requests.put(url, data)

    except requests.exceptions.ConnectionError:
        return "No connection to device."
    except:
        # raise
        return "Sorry, I don\'t know that device."

    return random.choice(['OK', 'Okey', 'It is done'])


def switch_device_off(params):

    try:
        devices = params['device']

        for device in devices:

            url = '{}devices/{}/'.format(HOST, equipment[device])
            data = {"name": device, "status": False}
            r = requests.put(url, data)

    except requests.exceptions.ConnectionError:
        return "No connection to device."
    except:
        # raise
        return "Sorry, I don\'t know that device."

    return random.choice(['OK', 'Okey', 'It is done'])


def play_music(params):
    os.system("mpg321 records/despacito.mp3")
