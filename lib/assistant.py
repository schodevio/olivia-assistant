import os.path
import sys

import json
from datetime import datetime

try:
    import apiai
except ImportError:
    sys.path.append(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
    )
    import apiai


class Assistant:

    def __init__(self):
        self.ai = apiai.ApiAI('6f15ee6447cc48598b5f2cd5edffbe24')

    def respond(self, text):
        request = self.ai.text_request()
        request.lang = 'en'  # optional, default value equal 'en'
        request.session_id = 'assistant001_{:%Y_%m_%d}'.format(datetime.now())

        request.query = text
        result = json.loads(request.getresponse().read())

        response = {
            'timestamp': result['timestamp'],
            'action':    result['result']['action'],
            'params':    result['result']['parameters'],
            'speech':    result['result']['fulfillment']['speech']
        }

        return response
